const bcryptjs = require('bcryptjs')
const User = require('../models/User')

async function login(req, res) {
  res.status(200).json({
    login: 'from controller',
  })
}

async function register(req, res) {
  const candidate = await User.findOne({
    email: req.body.email,
  })

  if (candidate) {
    res.status(409).json({
      message: 'Такой email уже занят. Попробуйте другой',
    })
  } else {
    const salt = bcryptjs.genSaltSync(10)
    const password = req.body.password
    console.log(salt, password)
    const user = new User({
      email: req.body.email,
      password: bcryptjs.hashSync(password, salt),
    })
    try {
      await user.save()
      res.status(201).json(user)
    } catch (e) {

    }
  }
}

module.exports = {register, login}
