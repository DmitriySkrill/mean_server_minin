module.exports.getByCategoryId = (req, res) => {
  res.status(200).json({
    getByCategoryId: 'from controller',
  })
}

module.exports.remove = (req, res) => {
  res.status(200).json({
    remove: 'from controller',
  })
}

module.exports.create = (req, res) => {
  res.status(200).json({
    create: 'from controller',
  })
}

module.exports.update = (req, res) => {
  res.status(200).json({
    update: 'from controller',
  })
}
